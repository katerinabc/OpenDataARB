numpy==1.26.3
pandas==2.1.4
plotly==5.18.0
python-dateutil==2.8.2
regex==2023.8.8
requests==2.31.0
