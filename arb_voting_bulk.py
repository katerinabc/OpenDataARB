# test file for playing with graphql and getting snapshot data

# help files:
# https://github.com/loross14/snapshot-voting-rates/blob/main/vote.py

# library

import json
import requests
import time
import pandas as pd
import timing

#url for connecting to snapshot API
url = "https://hub.snapshot.org/graphql"

# #create query string 
# # TODO: check: why is the mmeber list empty?

# space_query = """
# query space {
#     space(id: "arbitrumfoundation.eth"){
#     id
#     name
#     about
#     network
#     symbol
#     members
#     }
# }
# """

# #request data and parse out the space data
# r = requests.post(url, json={'query': space_query}).text
# r = json.loads(r)["data"]["space"]

print("getting all the closed proposals")

proposals = []
for i in range(0,5000,1000):
        
        print("Round {number_i} of fetching proposals".format(number_i = (i/1000)+1))
        
        proposal_query = ("""query Proposals {
        proposals (
            first: 1000
            skip: """) + str(i) + ("""
            where: {
                space_in: ["arbitrumfoundation.eth"],
                state: "closed"
            },
            orderBy: "created",                                               
            orderDirection: desc
            ) {
              id
              title
              body
              choices
              start
              end
              snapshot
              state
              scores
              scores_by_strategy
              scores_total
              scores_updated
              author
            }
          }""")
        # print(proposal_query)

        #make request for 1000 votes at a time
        r = requests.post(url, json={'query': proposal_query})
        # print(r.text)
        r = json.loads(r.text)["data"]["proposals"]

        #add results to ongoing list
        for proposal in r:
            proposals.append(proposal)
      
        #wait to avoid overloading the endpoint
        time.sleep(1)
                                                          
print("{nbr_probs} proposals.".format(nbr_probs= len(proposals)))

prbsDF = pd.DataFrame.from_dict(proposals)
l_proposals = prbsDF.shape[0]

print("Get the votes for each proposal")
# for each proposal, construct multiple queries to gather all of the votes.
# note that the skip feature of the votes query has a maximum value of 5000, while some proposals are currently getting >8k votes.
# in order to account for this limiting factor, I run 2 loops: the first descending, the second ascending, and then remove duplicates.

# proposals = prbsDF.id.to_list()
voters = []
# voters_dict = {}  --> initiate with first votes for it to work?

p = 1
last_block = min(prbsDF['start']) -1
prop_step = 10
for i in range(0, l_proposals, prop_step):
    print("slice 1:", i, "slice 2: ", i+prop_step)

    time.sleep(1)

    for row in prbsDF[i:(i+prop_step)].itertuples(index=False):
        # for each proposal, get all the votes
        # get the votes by getting the first 1000 votes, then check the time of the last block
        # then get the next 1000 votes, making sure that that they have been casted later
        print("Doing: Proposal {first} to proposal {last}. Total of {length} proposals to get".format(first = i, last = (i+prop_step), length = l_proposals))
        
        filename = "proposal{p}.json".format(p = p)
        
        time.sleep(1)

        # loop through the votes on proposal getting them in batches of 1000
        # the loop is running until created = proposal end day

        proposal = row[0]
        startT = row[4]
        endT = row[5]
        print("proposal id {proposal}, start block {startT}, end block {endT}".format(proposal = proposal, startT = startT, endT= endT))

        # set the last block to the first block of the proposal
        last_block = startT

        while last_block < endT:
            block_togo = endT - last_block
            print("{last_block} block. {block_togo} to go.".format(last_block = last_block, block_togo = block_togo))

            #make request for 1000 votes at a time
            # check if more or less than 1000 blocks to go
            if block_togo > 1000:
                take_votes = 1000
            else:
                take_votes = block_togo
            vote_query = vote_query = ("""query Votes {
            votes (
                first: """) + str(take_votes) + ("""
                skip: 0
                where: {
                    proposal:""") + ' "' + proposal + '"' + ("""
                    created_gt: """) + str(last_block) + ("""
                }
                orderBy: "created",
                orderDirection: asc
            ) {
                voter
                vp
                created
                proposal {id}
                choice
                space {id}
            }
            }""")
            # print(vote_query)

            response = requests.post(url, json={'query': vote_query})

            result = json.loads(response.text)["data"]["votes"]

            #last block
            # if result is empty (no new votes) then make last_block the end date of voting
            try:
                #add results to ongoing dictionary
                for vote in result:
                    voters.append(vote)
                        
                createdT = [d.get('created') for d in result]
                
                # figure out what the last block was
                last_block = max(createdT)
            except:
                last_block = endT
            print("last block:", last_block)

            #wait to avoid overloading the endpoint
            time.sleep(1)
        
        # counter for proposals
        p = p + 1

        #wait to avoid overloading the endpoint
        time.sleep(5)

    print("Transform data into json and save it")
    # write results to a json file
    filname = "proposal{slice1}to{slice2}.json".format(slice1 = i, slice2 = (i+prop_step))
    with open(filname, mode="w", encoding="utf-8") as outfile:
        # Start of json_arr
        outfile.write("[\n")

        # Index of last element in json_arr
        voters_last = len(voters) - 1

        for index, element in enumerate(voters):
            # Add indentation
            outfile.write("    ")

            # Write element
            json.dump(element, outfile, indent=4)

            # Add comma if not last element
            if index != voters_last:
                outfile.write(",")

            # Add newline
            outfile.write("\n")

        # End of json_arr
        outfile.write("]\n")

        #wait to avoid overloading the endpoint
        time.sleep(10)

# read the data
# with open ("data.json", "r")  as f:
#     all_data = json.loads(f.read()) 

# # transform to pandas
# df = pd.DataFrame.from_dict(all_data)

