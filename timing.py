import atexit
import time
# from time import clock
from functools import reduce

def secondsToStr(t):
    return "%d:%02d:%02d.%03d" % \
        reduce(lambda ll,b : divmod(ll[0],b) + ll[1:],
            [(t*1000,),1000,60,60])

line = "="*40
def log(s, elapsed=None):
    print(line)
    print(secondsToStr(time.time()), '-', s)
    if elapsed:
        print("Elapsed time:", elapsed)
    print(line)
    print

def endlog():
    end = time.time()
    elapsed = end-start
    log("End Program", secondsToStr(elapsed))

def now():
    return secondsToStr(time.time())

start = time.time()
atexit.register(endlog)
log("Start Program")