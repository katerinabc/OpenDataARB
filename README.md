# OpenDataARB


## Installation
Run `requirements.txt` to install the requirements for this file. 

## Usage
The file downloads all voting on all closed proposals for Arbitrum. Votes are saved in a series of small files to save memory space. You can change how many proposals are grouped into one json file by changing `prop_step`.

To read the data uncomment the last couple of license

```
# read the data
# with open ("data.json", "r")  as f:
#     all_data = json.loads(f.read()) 

# # transform to pandas
# df = pd.DataFrame.from_dict(all_data)
```

## Data
Data from 144 closed proposal available [here](https://pink-mad-heron-685.mypinata.cloud/ipfs/QmSBUQ8d8DuDs7ZtrouFoFfXsavh2aUt8m5saYmHHcFrBF) for download. Unzip it and then use the code mentioned above to load the json files and transform them into a pandas dataframe for further processing. 


## License
Open source license. Feel free to contribute and reuse

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
